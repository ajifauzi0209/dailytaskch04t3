package com.kazakimaru.dailytaskch04t3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.kazakimaru.dailytaskch04t3.adapter.GameAdapter
import com.kazakimaru.dailytaskch04t3.databinding.FragmentGameBinding
import com.kazakimaru.dailytaskch04t3.model.Game


class GameFragment : Fragment() {

    private var _binding: FragmentGameBinding? = null
    private val binding get() = _binding!!

    private val listGame = arrayListOf<Game>(
        Game(R.drawable.banner_codwz, "Call of Duty: Warzone", "Genre: Shooter"),
        Game(R.drawable.banner_dyinglight2, "Dying Light 2", "Genre: Survival Horror"),
        Game(R.drawable.banner_pubg, "Playerunknown Battlegrounds", "Genre: Battle Royale"),
        Game(R.drawable.banner_visage, "Visage", "Genre: Horror"),
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initRecyclerView() {
        val gameAdapter = GameAdapter(listGame)
        binding.rvData.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = gameAdapter
        }
    }

}