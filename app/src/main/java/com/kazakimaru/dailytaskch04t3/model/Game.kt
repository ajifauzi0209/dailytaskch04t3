package com.kazakimaru.dailytaskch04t3.model

data class Game(
    val thumbnailImg: Int,
    val titleGame: String,
    val genreGame: String
)
