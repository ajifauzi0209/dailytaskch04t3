package com.kazakimaru.dailytaskch04t3.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kazakimaru.dailytaskch04t3.R
import com.kazakimaru.dailytaskch04t3.model.Game

class GameAdapter(val listGame: ArrayList<Game>): RecyclerView.Adapter<GameAdapter.GameViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_game, parent, false)
        return GameViewHolder(view)
    }

    // Melakukan penetapan data yang akan ditampilkan pada setiap item/baris
    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        holder.bind(listGame[position])
    }

    // Memberitahu banyaknya list yang akan ditampilkan
    override fun getItemCount(): Int = listGame.size

    // Class holder
    inner class GameViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val thumbnailGame: ImageView = view.findViewById(R.id.thumbnail_game)
        val txt_title: TextView = view.findViewById(R.id.title_game)
        val txt_genre: TextView = view.findViewById(R.id.genre_game)

        fun bind(item: Game) {
            thumbnailGame.setImageResource(item.thumbnailImg)
            txt_title.text = item.titleGame
            txt_genre.text = item.genreGame
        }
    }

}